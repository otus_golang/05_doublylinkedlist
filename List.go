package doublyLinkedList

type List struct {
	first      *Item
	last       *Item
	itemsCount uint
}

func (l *List) Len() uint {
	return l.itemsCount
}

func (l *List) First() *Item {
	return l.first
}

func (l *List) Last() *Item {
	return l.last
}

func (l *List) PushFront(v interface{}) {
	newItem := Item{v, l.first, nil, l}

	if l.First() == nil {
		newItem.next = nil
		l.first = &newItem
		l.last = &newItem
	} else {
		l.first.prev = &newItem
		l.first = &newItem
	}

	l.itemsCount += 1
}

func (l *List) PushBack(v interface{}) {
	if l.last == nil {
		l.PushFront(v)
	} else {
		newItem := Item{v, nil, l.last, l}

		l.last.next = &newItem
		l.last = &newItem
	}

	l.itemsCount += 1
}
