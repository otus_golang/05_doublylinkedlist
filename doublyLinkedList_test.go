package doublyLinkedList

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

var l List

func init() {
	l = List{}
}

func TestDoublyLinkedList(t *testing.T) {
	firstEl := "first"
	lastEl := "last"
	toRemoveEl := "to remove"

	var expectedLen uint

	assert.Equal(t, expectedLen, l.Len())

	l.PushFront(firstEl)
	expectedLen++
	assert.Equal(t, expectedLen, l.Len())

	l.PushBack(lastEl)
	expectedLen++
	assert.Equal(t, expectedLen, l.Len())

	assert.Equal(t, firstEl, l.First().Value)
	assert.Equal(t, lastEl, l.Last().Value)

	l.PushBack(toRemoveEl)
	expectedLen++
	assert.Equal(t, expectedLen, l.Len())
	assert.NotEqual(t, lastEl, l.Last().Value)

	l.Last().Remove()
	expectedLen--
	assert.Equal(t, expectedLen, l.Len())
	assert.Equal(t, lastEl, l.Last().Value)
}
