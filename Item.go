package doublyLinkedList

type Item struct {
	Value interface{}
	next  *Item
	prev  *Item
	list  *List
}

func (i *Item) Next() *Item {
	if i != nil {
		return i.next
	} else {
		panic("Next element has nil pointer")
	}
}

func (i *Item) Prev() *Item {

	if i != nil {
		return i.prev
	} else {
		panic("Previous element has nil pointer")
	}
}

func (i *Item) Remove() {
	if i.prev == nil {
		i.list.first = i.next
	} else {
		i.prev.next = i.next
	}

	if i.next == nil {
		i.list.last = i.prev
	} else {
		i.next.prev = i.prev
	}

	i.list.itemsCount -= 1
}
